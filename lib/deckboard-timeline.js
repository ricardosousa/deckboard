
 /*********************************************************************\
 *
 *
 *   Copyright (c) 2013-2014 Ricardo Sousa
 *   [ http://ricardo.maiasousa.com ]
 * 
 *   This file is part of Deckboard.
 * 
 *   Deckboard is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of 
 *   the License, or (at your option) any later version.
 * 
 *   Deckboard is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 * 
 *   You should have received a copy of the GNU Affero General Public
 *   License along with Deckboard.
 *   If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 \*********************************************************************/


/*
 *	Timeline status manager/helper (for Deckboard's configuration page)
 *
 *	@author: Ricardo Sousa
 *	@since:  2014-01-14
 */


//////////

var Timeline = function(view){
	this.view = view;
	this.viewduration = -1;

}

Timeline.prototype.getCount = function(){ return this.view && this.view.pages? this.view.pages.length : 0 };

Timeline.prototype.updateViewDuration = function(){
	if(this.getCount()>0){
		this.viewduration= this.view.pages.reduce(function(c,x){return x.active?c+x.duration:c},0);
	}else{
		this.viewduration=0;
	}

}
Timeline.prototype.getCicle = function(startTime){
	if(this.viewduration<=0){
		this.updateViewDuration();
	}
	return (new Date().getTime() - startTime) % this.viewduration;
}

/**
 * return which page is currently displayed, the total number of pages, the pages statuses, the cicle phase and the total view duration
 *
 */
Timeline.prototype.currentStatus = function(startTime){
	var cicle = this.getCicle(startTime);
	var timeout = cicle;  // not used
	var viewduration= this.viewduration;
	var step=0;
	var ellapsed=0;
	var i = -1;
	var l = this.getCount();
	var statuses = l>0?this.view.pages.map(function(x){return x.active}):[];

	var result = {page:i, count:l, statuses:statuses, viewduration:viewduration, cicle:cicle};

	if(result.viewduration>0){
		do{
			i = (i+1)%l;
			
			if(this.view.pages[i].active){
				var duration = Math.abs(this.view.pages[i].duration || 5000);// || _configuration.defaultPageDuration);
				if(duration>cicle)
				 timeout =duration-cicle;
				step=duration;
				ellapsed=cicle;
				cicle = cicle - duration;				
			}

		}while(cicle>0);

		result.page=i;
		result.ellapsed=ellapsed;
		result.step = step;
	}

	return result;
}





//export for nodejs
module.exports = {
	instanceFor : function(view){ return new Timeline(view) }
}