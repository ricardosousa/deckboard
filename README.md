Deckboard:
A dynamic, configurable and expansible digital signage solution running on nodejs, capable of displaying different page subsets across multiple rooms and views/screens (i.e. context specific page lists, sincronized across all viewers).

Licensed under AGPL (Affero General Public Licence) version 3; please see
LICENSE.txt for more details.
For commercial purposes, please contact "ricardo [at] maiasousa [dot] com".
