
 /*********************************************************************\
 *
 *
 *   Copyright (c) 2013-2014 Ricardo Sousa
 *   [ http://ricardo.maiasousa.com ]
 * 
 *   This file is part of Deckboard.
 * 
 *   Deckboard is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of 
 *   the License, or (at your option) any later version.
 * 
 *   Deckboard is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 * 
 *   You should have received a copy of the GNU Affero General Public
 *   License along with Deckboard.
 *   If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 \*********************************************************************/


/*
 *	Deckboard main service entry point
 *
 *	@author: Ricardo Sousa
 *	@since:  2013-09-14
 */

/*

	PLEASE NOTE:
	This project started as a basic webpage scheduling service which 
	eventually evolved into a much more complex system. For this reason,
	a major code refactoring should be expected in a future release,
	since it makes no sense to have 80% of the service logic in a 
	single file with more than 1K lines of code.

 */



var serverStart = new Date().getTime();


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   IMPORTS
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var express = require('express');
var http = require('http');
var util = require('util');
var jade = require('jade');
var nosql = require('nosql').load(__dirname+'/deckboard.nosql');
var timeline = require('./lib/deckboard-timeline.js'); //TODO: check if should use __dirname


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   INITIALIZATION
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


var app = express();


//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');//config.allowedDomains);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}




app.use(express.compress());
app.use('/static', express.static(__dirname + '/static'));
app.use(allowCrossDomain);
app.use(express.bodyParser());



var _defaults = {
					defaultPageDuration:5000,
					defaultPagePreload:3000,
					minimumRefreshInterval:2, //seconds
					serverPort:7474,
					profiling:true,
					profilingInterval:300000,
					jadeOptions:{
									pretty:true,
									debug:false,
									compileDebug:false,

									menu:[
											{path:'/config',title:'Pages',icon:'list-alt'},
											//{path:'/view',title:'View',icon:'eye-open'},
											{path:'/settings',title:'Settings',icon:'cog'},
											{path:'/about',title:'About',icon:'question-sign'}
										],

									serverStart:new Date(serverStart).toString()
								}
				};



var _configuration = null;

nosql.one(function(x){return x.domain=='configuration'},function(doc){
	
	if(doc===null){


	  	_configuration= merge({domain:'configuration'}, _defaults);

	  	if(_configuration.profiling){
	  		console.log("creating configuration from default values: "+JSON.stringify(_configuration));
	  	}
	  	saveConfig(function(success){
	  		startDeckboard();
	  	});

	  }else{
	  	_configuration=merge(_defaults, doc); //update new default values, if not defined in configuration
	  	if(_configuration.profiling){
	  		console.log("configuration loaded: "+JSON.stringify(_configuration));
	  	}
	  	startDeckboard();
	  }

	  
});


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   AUXILIARY FUNCTIONS
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


var got = function(f,data){
	if(typeof f === 'function'){
		f(data);
	}else{
		console.log(" * WARNING: could not invoke function -- unexpected type: "+(typeof f));
	}
}


var httpOptions = function(targetPath,proxyHost,proxyPort) {
	// no proxy host/port defined
	if(proxyHost === undefined){
		return targetPath;
	}else{ //proxy defined
		proxyPort = proxyPort || 8080;
		return {
			 host: proxyHost,
			  port: proxyPort,
			  path: targetPath,
			  headers: { Host: targetPath }
			}
	}
};

var getHttp = function(options,success,error) {
	http.get(options, function(res) {
		res.setEncoding('utf8');
		var data = '';
		res.on('data', function (chunk) {
    		data += chunk;
  		});
  		res.on('end',function(){got(success,data)});

  		res.on('error',function(e){got(error,e.message)});

	});
};




var getAllRooms = function (success,error){	
	nosql.all(function(x){return x.domain=='rooms'},function(rooms){got(success,rooms)});
}


var getRoom = function (r,success,error){
	
	nosql.one(function(x){return x.domain=='rooms' && x.title ==r;},function(room){
		if(room===null){
		 got(error, 'Room "'+r+'"" not found');
		}else{
			got(success,room);	
		}

	});
}

var getRoomView = function (r,v,success,error){
	
	getRoom(r,
		function(room){
			var views = room.views.filter(function(eachView){
				return eachView.title==v;
			});

			if(views.length>0){
				got(success,views[0]);
			}else{
				got(error,'View "'+v+'" not found for Room "'+r+'"');
			}
		},
		function(err){got(error,err)});

}

var getRoomViewPage = function (r,v,p,success,error){
	
	getRoomView(r,v,
		function(view){
			var pages = view.pages.filter(function(eachPage){
				return eachPage.title==p;
			});

			if(pages.length>0){
				got(success,pages[0]);
			}else{
				got(error,'Page "'+p+'" not found for View "'+v+'" and Room "'+r+'"');
			}
		},
		function(err){got(error,err)});
}


var whichPageShown = function(r,v,success,error){


	getRoomView(r,v,
					function(view){
						var i = -1;
						var l = view.pages.length;
						if(l<=0){
							got(error,'no pages to display');
						}else{		
							var activepages = 0;
							var viewduration = view.pages.reduce(function(c,x){return x.active?c+x.duration:c},0);
							var cicle = (new Date().getTime() - serverStart) % viewduration;
							var timeout = cicle;


							if(viewduration<=0){
								got(error,'no active pages to display (total duration is zero)');
							}else{
								do{
									i = (i+1)%l;
									
									if(view.pages[i].active){
										var duration = Math.abs(view.pages[i].duration || _configuration.defaultPageDuration);
										if(duration>cicle)
										 timeout =duration-cicle;
										cicle = cicle - duration;
									}

								}while(cicle>0);

								var result = {page:view.pages[i], timeout:timeout};
								got(success,result);	

							}
						}
					},
					function(err){ got(error,err); }
				);

}


var getTimeline = function(r,v,success,error){


	getRoomView(r,v,
					function(view){
						got( success, timeline.instanceFor(view).currentStatus(serverStart) );	
					},
					function(err){ got(error,err); }
				);

}

var monitorServer = function(){
	console.log("\t\t\t\t\t\t["+new Date()+"] memory usage: "+util.inspect(process.memoryUsage()));
}


var saveConfig = function(success,error){

	nosql.one(function(x){return x.domain=='configuration'},function(doc){

		if(doc===null){
			nosql.insert(_configuration,function(){got(success,_configuration)});
		}else{
			nosql.update(function(y){ 
				if(y.domain=='configuration'){
					y=_configuration;
				}
				return y;
			 },function(){got(success,_configuration)});
		}
	});
	
}


var buildView = function(file,data){
	file=file || '404.jade';
	data = data || {};
	var options = merge(_configuration.jadeOptions, data);
	var html = jade.renderFile(__dirname+'/views/'+file, options);
	var layoutOptions = {pageContent:html};
	var layout = jade.renderFile(__dirname+'/views/layout.jade', merge(options,layoutOptions));
	
	return layout;
}

var merge = function(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

var removeUndefineds = function(obj){
	for (var attrname in obj) {
		if(obj[attrname]===undefined)
			delete obj[attrname];
		 
	}
}

var storeRoom = function (r,success,error){	

	getRoom(r,
			function(roomFound){
				got(error,"Room already exists");
			},
			function(roomNotFound){
				console.log('Creating room "'+r+'"');
				var room = {
					domain:'rooms',
					title:r,
					active:true,
					views:[]
				};

				nosql.insert(room,function(){
					got(success,room);
				});
			});

	
}


var storeRoomView = function (r,v,success,error){	

	console.log('Creating view "'+v+'" for room "'+r+'"');
	var view = {
		title:v,
		active:true,
		pages:[]
	};


	getRoomView(r,v,
				function(viewFound){ got(error,"View already exists"); },
				function(viewNotFound){
					getRoom(r,
							function(roomFound){
								nosql.update(function(eachRoom){
									if(eachRoom.domain=='rooms' && eachRoom.title==r){
										eachRoom.views.push(view);
									}
									return eachRoom;
								},function(){ getRoomView(r,v,success,error) });
							},
							function(roomNotFound){
								storeRoom(r,
										  function(roomStored){
										  	//Try again
										  	storeRoomView(r,v,success,error);
										  },
										  function(storeErr){
										  	got(error,storeErr);
										  }
										 );
							});

				});

}

var storeRoomViewPage = function (r,v,pname,p,success,error){	

	console.log('Creating/Editing page "'+pname+'" on view "'+v+'" for room "'+r+'"');

	var page = {
		title:p.title,
		url:p.url,
		duration:p.duration,
		preload:p.preload,
		active:p.active
	};

	if(pname===undefined || pname.trim().length<=0){
		got(error,"Invalid page");
	}else if(page.title===undefined || page.title.trim().length<=0){
		got(error,"Invalid page title");
	}else if(page.url===undefined || page.url.trim().length<=0){
		got(error,"Invalid page URL");
	}else if(page.duration===undefined || page.duration<=0){
		got(error,"Invalid page duration");
	}else if(page.preload===undefined 	|| page.preload<0){
		got(error,"Invalid page preload time");
	}else if(page.active===undefined){
		got(error,"Invalid page state");
	}else{	

		var continueStoringPage = function(deleteOldEntry){

			var changedN = 0;
			getRoomView(r,v,
					function(viewFound){
						nosql.update(function(eachRoom){
							if(eachRoom.domain=='rooms' && eachRoom.title==r){	//Room r


								eachRoom.views.forEach(function(eachView){
									if(eachView.title==v){	//View v

										
										if(deleteOldEntry){	//Delete old entry
											for(var i = eachView.pages.length - 1; i >= 0; i--) {
												if(eachView.pages[i].title === pname) {
													eachView.pages.splice(i, 1);
												}
											}
										}

										eachView.pages.forEach(function(eachPage){
											if(eachPage.title==p.title){ //Page p
												eachPage.url=p.url;
												eachPage.duration=p.duration;
												eachPage.active=p.active;
												eachPage.preload=p.preload;
												changedN++;
											}
										});

										if(changedN<=0){
											eachView.pages.push(page);
											changedN++;
										}
									}
								});

							}

							return eachRoom;
						},
						function(){
							if(changedN>0) got(success,page);
							else got(error,"Nothing updated");
						});
					},
					function(viewNotFound){
						storeRoomView(r,v,
											  function(viewStored){
											  	//Try again
											  	storeRoomViewPage(r,v,p,success,error);
											  },
											  function(storeErr){
											  	got(error,storeErr);
											  }
											 );
					}
			);
		}

		if(pname==page.title){ //title unchanged (new or edited page)
			continueStoringPage(false);
		}else{ //title has changed (probably edited, but can be a new page)
			getRoomViewPage(r,v,p.title,
				function(pageAlreadyExists){got(error,'Another page with this title ("'+p.title+'") already exists for current view')},
				function(pageWithNewTitleDoesNotExist){
					continueStoringPage(true);
				});
		}


	}

}



var removeRoom = function (r,success,error){	
	console.log('Removing room "'+r+'"');
	var room = undefined;
	nosql.remove(function(eachRoom){
					return eachRoom.domain=='rooms' && eachRoom.title==r && (room = eachRoom || true);
				},function(removed){
					if(removed>0) got(success,room);
					else got(error,"No room found to be removed");
				});

}

var removeRoomView = function (r,v,success,error){	
	console.log('Removing view "'+v+'" on room "'+r+'"');

	var changedN = 0;
	var view = undefined;
	getRoom(r,
			function(roomFound){
				nosql.update(function(eachRoom){
					if(eachRoom.domain=='rooms' && eachRoom.title==r){	//Room r

						for(var i = eachRoom.views.length - 1; i >= 0; i--) {	//Delete entry
							if(eachRoom.views[i].title==v){
								view = eachRoom.views[i];
								eachRoom.views.splice(i,1);
								changedN++;
							}
						}

					}

					return eachRoom;
				},
				function(){
					if(changedN>0) got(success,view);
					else got(error,"No view found to be removed");
				});
			},
			function(roomNotFound){
				got(error,roomNotFound);
			}
	);	
}

var removeRoomViewPage = function (r,v,p,success,error){	

	console.log('Removing page "'+p+'" on view "'+v+'" for room "'+r+'"');

	var changedN = 0;
	var page = undefined;
	getRoomView(r,v,
			function(viewFound){
				nosql.update(function(eachRoom){
					if(eachRoom.domain=='rooms' && eachRoom.title==r){	//Room r

						eachRoom.views.forEach(function(eachView){
							if(eachView.title==v){	//View v

								for(var i = eachView.pages.length - 1; i >= 0; i--) {//Delete entry
									if(eachView.pages[i].title === p) {
										page = eachView.pages[i];
										eachView.pages.splice(i, 1);
										changedN++;
									}
								}

							}
						});

					}

					return eachRoom;
				},
				function(){
					if(changedN>0) got(success,page);
					else got(error,"No page found to be removed");
				});
			},
			function(viewNotFound){
				got(error,viewNotFound);
			}
	);


}


var sortRoomViewPages = function (r,v,ps,success,error){

	getRoomView(r,v,
		function(view){

			if(view.pages.length!=ps.length){
				got(error,"page lengths do not match");
			}else{
				
				//Check if page names match
				var pageNames= new Array();
				var sortedIndexes = new Array();

				view.pages.map(function(each){
					pageNames.push(each.title);
				});

				var matchCount=pageNames.length;
				for(var i = 0; i< ps.length; i++) {
					var j=pageNames.indexOf(ps[i]);
					if( j >= 0) {
						matchCount--;
						sortedIndexes.push(j);
					}
				}

				if(matchCount>0){
					got(error,"page entries do not match");
				}else{

					//Sort pages
					var sortedPages = new Array();
					for(var i=0; i<sortedIndexes.length; i++){
						sortedPages.push(view.pages[sortedIndexes[i]]);
					}

					var changedN=0;

					nosql.update(function(eachRoom){
							if(eachRoom.domain=='rooms' && eachRoom.title==r){	//Room r

								eachRoom.views.forEach(function(eachView){
									if(eachView.title==v){	//View v
										eachView.pages=sortedPages;
										changedN++;
									}
								});

							}

							return eachRoom;
						},
						function(){
							if(changedN>0) got(success,ps);
							else got(error,"Nothing updated");
						});

				}


			}

		}
		,error);
}	


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   SET ROUTES
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



app.get('/', function(req, res){
  res.redirect('/config');
});


//	LIST ROOMS
//-------------------
app.get('/r', function(req, res){


	getAllRooms(
		function(rooms){
			var result = new Array();
			rooms.forEach(function(x){ result.push(x.title)});
			res.json(result);
		},
		function(err){res.status(404).send(err);
	});
	
});



//	LIST VIEWS
//-------------------
app.get('/r/:room/v', function(req, res){
	

	getRoom(
			req.params.room,
			function(room){
					var result = new Array();
					if(room.views!==undefined) room.views.forEach(function(x){ result.push(x.title)});
				res.json(result);
			},
			function(err){
				res.status(404).send(err);
			}
		);
});


//	LIST URLS
//-------------------
app.get('/r/:room/v/:view/u', function(req, res){


	getRoomView(
			req.params.room,
			req.params.view,
			function(view){
					var result = new Array();
					if(view.pages!==undefined) view.pages.forEach(function(x){ result.push(x.url)});
				res.json(result);
			},
			function(err){
				res.status(404).send(err);
			}
		);

});

//	WHICH URL
//-------------------

app.get('/r/:room/v/:view/w', function(req, res){

	whichPageShown(
			req.params.room,
			req.params.view,
			function(data){
				res.json(data);
			},
			function(err){
				res.status(404).send(err);
			}
			);
});


//	GET TIMELINE INFO
//-------------------

app.get('/r/:room/v/:view/t', function(req, res){

	getTimeline(
			req.params.room,
			req.params.view,
			function(data){
				res.json(data);
			},
			function(err){
				res.status(404).send(err);
			}
			);
});



//	SHOW CURRENT PAGE
//-------------------

app.get('/r/:room/v/:view/s', function(req, res){

	var pStart=new Date().getTime();
	whichPageShown(
			req.params.room,
			req.params.view,
			function(data){
				var options = httpOptions(data.page.url,data.page.proxyHost,data.page.proxyPort);
				getHttp(options,function(result){
					res.header("refresh", Math.max(Math.ceil(data.timeout/1000),_configuration.minimumRefreshInterval));
					res.send(result);

					if(_configuration.profiling){
						console.log(" >> ["+req.params.room+"/"+req.params.view+"] processed in "+(new Date().getTime()-pStart)+" ms");
					}
				});
			},
			function(err){
				res.header("refresh", 10);
				res.status(404).send(err);
			}
			);
});



//	SHOW CURRENT PAGE IN FRAME
//-------------------

app.get('/r/:room/v/:view/f', function(req, res){

	whichPageShown(
			req.params.room,
			req.params.view,
			function(data){

				res.header("refresh", Math.max(Math.ceil(data.timeout/1000),_configuration.minimumRefreshInterval));
				var result='<html><body style="margin:0">'+
								'<iframe src="'+data.page.url+'" style="border:0;width:100%;height:100%"></iframe>'
						   '</body</html>';
				res.send(result);

			},
			function(err){
				res.header("refresh", 10);
				res.status(404).send(err);
			}
			);
});



//	SHOW CONFIGURATION PAGE
//-------------------
app.get('/config', function(req, res){ showConfigPage(req,res) });
app.get('/config/*', function(req, res){ showConfigPage(req,res) });

var showConfigPage = function(req,res){

	req.params[0]=req.params[0]||"";
	
	// Build breadcrumb
	var cpath=req.params[0].split('/').filter(function(t){return t.length>0});
	var cpathBuilt = [];
	var cpathAcc = '/config';
	for(var i=0;i<cpath.length;i++){
		var thisPath = {};
		thisPath.title = cpath[i];
		cpathAcc = cpathAcc + '/' + cpath[i];
		thisPath.path = cpathAcc;
		cpathBuilt.push(thisPath);
	}

	var cRoom = cpath[0];
	var cView = cpath[1];
	var cPage = cpath[2];

	var cOptions =
					{
						currentPage:'/config',
						pageTitle:"Configure Deckboard pages",
						configPath:cpathBuilt,
						chosenRoom:cRoom,
						chosenView:cView,
						chosenPage:cPage
						};


	if(cPage){			//Room, view and page chosen
		getRoomViewPage(cRoom,cView,cPage,
				function(page){
					cOptions.chosenUrl = page.url;
					cOptions.chosenDuration = page.duration;
					cOptions.chosenPreload = page.preload;
					cOptions.chosenActive = page.active;

					getRoomView(cRoom,cView,
								function(view){
									cOptions.viewPages = view.pages;
									res.send(buildView('config_pages.jade',	cOptions ));
								},
								function(error){
									console.log("UNEXPECTED ERROR: "+error);
									res.send(buildView('config_pages.jade',	cOptions ));
									} //FIXME
								);
				},
				function(pageNotFound){ //in this case, we are creating a new page
					cOptions.creatingPage=true;
					cOptions.chosenUrl='';
					cOptions.chosenDuration=_configuration.defaultPageDuration;
					cOptions.chosenPreload=_configuration.defaultPagePreload;
					cOptions.chosenActive=true;

					getRoomView(cRoom,cView,
								function(view){
									cOptions.viewPages = view.pages;
									res.send(buildView('config_pages.jade',	cOptions ));
								},
								function(error){
									console.log(" ERROR: "+error);
									res.send(buildView('config_pages.jade',	cOptions ));
									} //FIXME:redirect to /config/room/view?error=..
								);
					}
				);


	}else if(cView){	//Room and view chosen
		getRoomView(cRoom,cView,
				function(view){
					cOptions.viewPages = view.pages;
					res.send(buildView('config_pages.jade',	cOptions ));
				},
				function(error){
					console.log("ERROR: "+error);
					res.send(buildView('config_pages.jade',	cOptions ));
					} //FIXME
				);


	}else if(cRoom){	//Room chosen
		getRoom(cRoom,
				function(room){
					cOptions.roomViews = room.views;
					res.send(buildView('config_pages.jade',	cOptions ));
				},
				function(error){
					console.log("ERROR: "+error);
					res.send(buildView('config_pages.jade',	cOptions ));
					} //FIXME
				);


	}else{				//Nothing chosen
		getAllRooms(
				function(rooms){
					cOptions.deckRooms = rooms;
					res.send(buildView('config_pages.jade',	cOptions ));
				},
				function(error){
					console.log("ERROR: "+error);
					res.send(buildView('config_pages.jade',	cOptions ));
					} //FIXME
				);
	}

	
}



// ADD ROOMS
//-------------------
app.post('/config/:room', function(req, res){
	  var roomName = req.params.room.trim();

	  if(roomName.length<=0)
	  	  res.status(500).send("Invalid room name");
	  else
	  	  storeRoom(roomName,function(success){res.send("Room created")},function(error){res.status(500).send(error)});
	});

// DELETE ROOMS
//-------------------
app.del('/config/:room', function(req, res){
	  var roomName = req.params.room.trim();

	  if(roomName.length<=0)
	  	  res.status(500).send("Invalid room name");
	  else
	  	  removeRoom(roomName,function(success){res.send("Room created")},function(error){res.status(500).send(error)});
	});



// ADD VIEWS
//-------------------
app.post('/config/:room/:view', function(req, res){
	  var roomName = req.params.room.trim();
	  var viewName = req.params.view.trim();

	  if(roomName.length<=0 || viewName.length<=0)
	  	  res.status(500).send("Invalid room or view name");
	  else
	  	 storeRoomView(roomName,viewName,function(success){res.send("View created")},function(error){res.status(500).send(error)});

	});


// DELETE VIEWS
//-------------------
app.del('/config/:room/:view', function(req, res){
	  var roomName = req.params.room.trim();
	  var viewName = req.params.view.trim();

	  if(roomName.length<=0 || viewName.length<=0)
	  	  res.status(500).send("Invalid room or view name");
	  else
	  	 removeRoomView(roomName,viewName,function(success){res.send("View created")},function(error){res.status(500).send(error)});

	});



// ADD PAGES
//-------------------
app.post('/config/:room/:view/:page', function(req, res){
	  var roomName = req.params.room.trim();
	  var viewName = req.params.view.trim();
	  var pageName = req.params.page.trim(); //current page name; may be diferent from the new title

	  var newPage  = {
	  					title : (''+req.body.titleInput).trim(),
	  					url : (''+req.body.urlInput).trim(),
	  					duration : parseInt('0'+req.body.durationInput),
	  					preload : parseInt('0'+req.body.preloadInput),
	  					active : (''+req.body.activeInput).trim()=='on'
	  				};

	  if(roomName.length<=0 || viewName.length<=0 || pageName.length<=0)
	  	  res.status(500).send("Invalid room, view or page name");
	  else
	  	 storeRoomViewPage(roomName,viewName,pageName,newPage,
	  	 	function(success){res.redirect('/config/'+roomName+'/'+viewName+'/'+newPage.title)},
	  	 	function(error){res.status(500).send(error)}); //FIXME

	});

// REMOVE PAGES
//-------------------

var doDelete = function(req,res){
	var roomName = req.params.room.trim();
	var viewName = req.params.view.trim();
	var pageName = req.params.page.trim();

	if(roomName.length<=0 || viewName.length<=0 || pageName.length<=0)
		res.status(500).send("Invalid room, view or page name");
	else
		removeRoomViewPage(roomName,viewName,pageName,
			function(success){res.redirect('/config/'+roomName+'/'+viewName)},
			function(error){res.status(500).send(error)}); //FIXME
}

app.delete('/config/:room/:view/:page', doDelete);
app.post('/config/:room/:view/:page/DELETE', doDelete);


// SORT PAGES
//-------------------

app.post('/sort/:room/:view', function(req, res){ 
	
	var roomName = req.params.room.trim();
	var viewName = req.params.view.trim();
	var sortedEntries = req.body;

	sortRoomViewPages(roomName,viewName,sortedEntries,
			function(success){res.send(success)},
			function(error){res.status(500).send(error);});
});


//	SHOW ABOUT PAGE
//-------------------
app.get('/about', function(req, res){
	var cOptions =
					{
						currentPage:'/about',
						pageTitle:"About Deckboard"
						};
	res.send(buildView('about.jade',	cOptions ));
});


//	VIEW DECKBOARD
//-------------------
app.get('/view/:room/:view',function(req,res){

	var roomName = req.params.room.trim();
	var viewName = req.params.view.trim();
	var data = {room:roomName,view:viewName};
	var options = merge(_configuration.jadeOptions, data);
	res.send(jade.renderFile(__dirname+'/views/deckview.jade',options));

});


//	SETTINGS PAGE
//-------------------
app.get('/settings', function(req, res){
	var cOptions =
					{
						currentPage:'/settings',
						pageTitle:"Deckboard configuration",
						config:_configuration,
						defconfig:_defaults
						};
	res.send(buildView('settings.jade',	cOptions ));
});

app.post('/settings', function(req, res){

	try{


		var newCfg = {
						serverPort:parseInt(req.body.serverPort)||undefined,
						defaultPageDuration:req.body.defaultPageDuration||undefined,
						defaultPagePreload:req.body.defaultPagePreload||undefined,
						minimumRefreshInterval:req.body.minimumRefreshInterval||undefined
					 };

		removeUndefineds(newCfg);

		updatedCfg = merge(_configuration,newCfg);
		_configuration=updatedCfg;

		saveConfig(function(success){
			res.redirect("/settings");
		});

	}catch(err){
		console.log("error saving settings:"+err);
		res.redirect("/settings");
	}
	
});


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   START
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var startDeckboard = function(){
	app.listen(_configuration.serverPort);

	console.log("\n\n - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
	console.log("|      DeckBoard started in "+(new Date().getTime()-serverStart)+" ms, listening on port "+_configuration.serverPort);
	console.log(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n");
	console.log("App routes:");
	app.routes.get.forEach(function(x){console.log("   ·  [GET] \t" + x.path)});
	app.routes.post.forEach(function(x){console.log("   ·  [POST] \t" + x.path)});
	console.log("\n\n");

	if(_configuration.profiling){
		setInterval(monitorServer,_configuration.profilingInterval);
	}
}
